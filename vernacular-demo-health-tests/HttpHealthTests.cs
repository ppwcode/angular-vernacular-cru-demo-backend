using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace vernacular_demo_health_tests
{
    [TestFixture]
    public class HttpHealthTests
    {
        [Test]
        public async Task Call_Azure_Function_Running_In_Azure_Test()
        {
            // Arrange
            var httpClient = HttpClientFactory.Create();
            Uri functionUri = new Uri("https://vernacular-health.azurewebsites.net/api/I/health?code=becS8v4kpkGqWvzC9HoFC/AIuSBMkB6PY/fV7aoi6EfAHjdEWGLGgg==");

            // Act
            httpClient.DefaultRequestHeaders.Add("x-flow-id", "72bd9328-3e3e-46da-93da-bff6a66d275d");
            httpClient.DefaultRequestHeaders.Add("x-mode", "automated-test-cbf23c05-2ed0-4fe2-8247-241aa8f0aa75");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + await GetBearerToken());
            var response = await httpClient.GetAsync(functionUri);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task Call_Azure_Function_Running_In_Azure_With_Invalid_Bearer_Token_Test()
        {
            // Arrange
            var httpClient = HttpClientFactory.Create();
            Uri functionUri = new Uri("https://vernacular-health.azurewebsites.net/api/I/health?code=becS8v4kpkGqWvzC9HoFC/AIuSBMkB6PY/fV7aoi6EfAHjdEWGLGgg==");

            // Act
            httpClient.DefaultRequestHeaders.Add("x-flow-id", "72bd9328-3e3e-46da-93da-bff6a66d275d");
            httpClient.DefaultRequestHeaders.Add("x-mode", "automated-test-cbf23c05-2ed0-4fe2-8247-241aa8f0aa75");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + "123");
            var response = await httpClient.GetAsync(functionUri);

            // Assert
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task Call_Azure_Function_Running_In_Azure_With_Forced_Error_Test()
        {
            // Arrange
            var httpClient = HttpClientFactory.Create();
            Uri functionUri = new Uri("https://vernacular-health.azurewebsites.net/api/I/health?code=becS8v4kpkGqWvzC9HoFC/AIuSBMkB6PY/fV7aoi6EfAHjdEWGLGgg==");

            // Act
            httpClient.DefaultRequestHeaders.Add("x-flow-id", "72bd9328-3e3e-46da-93da-bff6a66d275d");
            httpClient.DefaultRequestHeaders.Add("x-mode", "automated-test-cbf23c05-2ed0-4fe2-8247-241aa8f0aa75");
            httpClient.DefaultRequestHeaders.Add("x-force-error", "500");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + await GetBearerToken());
            var response = await httpClient.GetAsync(functionUri);

            // Assert
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        private async Task<string> GetBearerToken()
        {
            var httpClient = HttpClientFactory.Create();
            Uri tokenUri = new Uri("https://login.microsoftonline.com/4356a4db-1bd7-486e-9e86-6913493ecdda/oauth2/v2.0/token");

            var headers = new List<KeyValuePair<string, string>>
            {
                new ("client_id", "b4768618-ee28-4323-8061-e5885ba08725"),
                new ("grant_type", "client_credentials"),
                new ("client_secret", "whg7Q~APe3nBcOwKNmZ.nrBO1hwPvL2Xqy0EN"),
                new ("scope", "b4768618-ee28-4323-8061-e5885ba08725/.default")
            };

            var request = new HttpRequestMessage(HttpMethod.Post, tokenUri) { Content = new FormUrlEncodedContent(headers) };
            var response = httpClient.SendAsync(request);
            var content = await response.Result.Content.ReadAsStringAsync();
            dynamic responseContent = JObject.Parse(content);

            return responseContent.access_token;
        }
    }
}
