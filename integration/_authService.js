const FormData = require('form-data')
const fetch = require('node-fetch')
const getCredentials = require('./_getCredentials')
const tokenUri = 'https://login.microsoftonline.com/4356a4db-1bd7-486e-9e86-6913493ecdda/oauth2/v2.0/token'

const bearerToken = async function getBearerToken () {
  const { client_id, client_secret } = getCredentials('bearerTokenCredentials')
  const formdata = new FormData()
  formdata.append('client_id', client_id)
  formdata.append('grant_type', 'client_credentials')
  formdata.append('client_secret', client_secret)
  formdata.append('scope', 'b4768618-ee28-4323-8061-e5885ba08725/.default')

  const options = { method: 'POST', body: formdata }
  const response = await fetch(tokenUri, options)
  const result = await response.json()
  return 'Bearer ' + result.access_token
}

const lessPrivilegedBearerToken = async function getLessPrivilegedBearerToken () {
  const { client_id, client_secret } = getCredentials('lessPrivilegedBearerTokenCredentials')
  const formdata = new FormData()
  formdata.append('client_id', client_id)
  formdata.append('grant_type', 'client_credentials')
  formdata.append('client_secret', client_secret)
  formdata.append('scope', 'b4768618-ee28-4323-8061-e5885ba08725/.default')

  const options = { method: 'POST', body: formdata }
  const response = await fetch(tokenUri, options)
  const result = await response.json()
  return 'Bearer ' + result.access_token
}

module.exports = {
  getBearerToken: bearerToken,
  getLessPrivilegedBearerToken: lessPrivilegedBearerToken
}
