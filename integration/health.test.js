/* eslint-env mocha */

const testName = require('./_testName')
const callService = require('./_callService')
const authService = require('./_authService')
const { v4: uuid } = require('uuid')
const Joi = require('joi')

const ISODateStringSchema = Joi.string().isoDate()
const forcedErrors = [401, 403, 470, 500, 522]

// result contains statusCode, body, headers
function resultShouldBeAsExpected (result, statusCode, flowId, mode) {
  result.should.be.an.Object()
  result.statusCode.should.equal(statusCode)
  result.headers.should.be.an.Object()
  result.headers['x-flow-id'].should.equal(flowId)
  result.headers['x-mode'].should.equal(mode)
  Joi.assert(result.headers['x-date'], ISODateStringSchema)
  result.headers['cache-control'].should.equal('no-store')
}

describe(testName(module), function () {
  this.timeout(10000)
  it('answers nominally', async function () {
    const flowId = uuid()
    const mode = `automated-test-${flowId}`
    const bearerToken = await authService.getBearerToken()
    const result = await callService('GET', {
      'x-flow-id': flowId,
      'x-mode': mode,
      Authorization: bearerToken.toString()
    })
    resultShouldBeAsExpected(result, 200, flowId, mode)
    JSON.parse(result.body).should.be.an.Object()
    Object.keys(JSON.parse(result.body)).length.should.be.equal(3)
  })
  it('responds 401 when the access token is invalid', async function () {
    const flowId = uuid()
    const mode = `automated-test-${flowId}`
    const result = await callService('GET', {
      'x-flow-id': flowId,
      'x-mode': mode,
      Authorization: 'test'
    })
    resultShouldBeAsExpected(result, 401, flowId, mode)
    JSON.parse(result.body).should.be.an.Object()
    Object.keys(JSON.parse(result.body)).length.should.be.equal(4)
  })
  it('responds 403 when the user does not have enough privileges', async function () {
    const flowId = uuid()
    const mode = `automated-test-${flowId}`
    const bearerToken = await authService.getLessPrivilegedBearerToken()
    const result = await callService('GET', {
      'x-flow-id': flowId,
      'x-mode': mode,
      Authorization: bearerToken.toString()
    })
    resultShouldBeAsExpected(result, 403, flowId, mode)
    JSON.parse(result.body).should.be.an.Object()
    Object.keys(JSON.parse(result.body)).length.should.be.equal(4)
  })
  describe('forced errors', function () {
    forcedErrors.forEach(forcedError => {
      it(`returns ${forcedError} when requested`, async function () {
        const flowId = uuid()
        const mode = `automated-test-${flowId}`
        const bearerToken = await authService.getBearerToken()
        const result = await callService('GET', {
          'x-flow-id': flowId,
          'x-mode': mode,
          Authorization: bearerToken.toString(),
          'x-force-error': forcedError
        })
        resultShouldBeAsExpected(result, forcedError, flowId, mode)
      })
    })
  })
})
