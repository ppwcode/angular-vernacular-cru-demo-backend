const BlobServerFactory = require('azurite/dist/src/blob/BlobServerFactory')
const BlobEnvironment = require('azurite/dist/src/blob/BlobEnvironment.js')
const { spawn } = require('child_process')
const { join } = require('path')
const { open, writeFile, mkdir } = require('fs/promises')

const funcPath = join(__dirname, '..', 'node_modules', 'azure-functions-core-tools', 'bin', 'func')
const funcDirectory = join(__dirname, '..', 'vernacular-demo-health-isolated')
const blobStorageDirectory = join(__dirname, '..', 'scratch', 'azurite')
const funcWaitTime = 20000 // ms

let funcProcess
let blob

if (process.env.npm_config_mode !== 'prod') {
  process.env.npm_config_mode = 'local'
}

async function shutdown () {
  console.log('Azurite Blob service is closing...')
  await blob.close()
  console.log('Azurite Blob service successfully closed')
}

if (process.env.npm_config_mode === 'local') {
  exports.mochaHooks = {
    beforeAll: [
      async function ensureLocalSettings () {
        try {
          await open('vernacular-demo-health-isolated/local.settings.json', 'wx')
          const azureFunctionsSettings = {
            IsEncrypted: false,
            Values: {
              AzureWebJobsStorage: process.env.AzureWebJobsStorage,
              FUNCTIONS_WORKER_RUNTIME: process.env.FUNCTIONS_WORKER_RUNTIME,
              AuthenticationAuthority: process.env.Values__AuthenticationAuthority,
              AuthenticationClientId: process.env.Values__AuthenticationClientId
            }
          }
          await writeFile('vernacular-demo-health-isolated/local.settings.json', JSON.stringify(azureFunctionsSettings))
          console.log('created local.settings.json')
        } catch (err) {
          if (err.code === 'EEXIST') {
            console.log('local.settings.json already exists')
            return
          }
          throw err
        }
      },
      async function () {
        const blobServerFactory = new BlobServerFactory.BlobServerFactory()
        const blobEnvironment = new BlobEnvironment.default()
        await mkdir(blobStorageDirectory, { recursive: true })
        blobEnvironment.flags.location = blobStorageDirectory
        blobEnvironment.flags.l = blobStorageDirectory
        blob = await blobServerFactory.createServer(blobEnvironment)
        const config = blob.config
        // We use logger singleton as global debugger logger to track detailed outputs cross layers
        // Note that, debug log is different from access log which is only available in request handler layer to
        // track every request. Access log is not singleton, and initialized in specific RequestHandlerFactory implementations
        // Enable debug log by default before first release for debugging purpose
        // TODO Logger.configLogger(config.enableDebugLog, config.debugLogFilePath);
        // Start server
        console.log(`Azurite Blob service is starting on ${config.host}:${config.port}`)
        await blob.start()
        console.log(`Azurite Blob service successfully listens on ${blob.getHttpServerAddress()}`)
        // Handle close event
        process
          .once('message', msg => {
            if (msg === 'shutdown') {
              shutdown()
            }
          })
          .once('SIGINT', () => shutdown())
          .once('SIGTERM', () => shutdown())
      },
      async function () {
        this.timeout(funcWaitTime + 100)
        console.log('Starting function …')
        funcProcess = spawn(funcPath, ['start'], { cwd: funcDirectory })
        return new Promise(resolve => {
          // wait before starting tests until really started
          funcProcess.stderr.on('data', data => {
            console.error(`FUNC: ${data}`)
          })
          funcProcess.stdout.on('data', data => {
            console.log(`FUNC: ${data}`)
            if (data.includes('Worker process started and initialized')) {
              console.log('Function started')
              resolve(funcProcess)
            }
          })
        })
      }
    ],
    afterAll: [
      async function () {
        await shutdown()
      },
      async function () {
        console.log('Stopping function …')
        funcProcess?.kill('SIGKILL')
        console.log('Function stopped')
      }
    ]
  }
}
