const fetch = require('node-fetch')

const dummy = false

let baseURI
let uri

if (process.env.npm_config_mode === 'prod') {
  baseURI = 'https://vernacular-health.azurefd.net'
  uri = 'health/api/I/health/?code=becS8v4kpkGqWvzC9HoFC/AIuSBMkB6PY/fV7aoi6EfAHjdEWGLGgg=='
} else {
  baseURI = 'http://localhost:7071/api'
  uri = 'I/health'
}
console.log({ mode: process.env.npm_config_mode })

function dummyCall (method, uri, headers, body) {
  return {
    statusCode: 200,
    headers: {
      'x-flow-id': headers['x-flow-id'],
      'x-mode': headers['x-mode'],
      'x-date': '2021-12-08T11:46:22.585Z',
      'cache-control': 'no-store'
    },
    body: {}
  }
}

function createHTTPCall () {
  return async function httpCall (method, headers, body) {
    const actualURI = `${baseURI}/${uri}`
    const options = { method, headers, body }
    console.log(`calling ${method} ${uri} on ${process.env.npm_config_mode} …`)
    const response = await fetch(actualURI, options)
    const rawHeaders = response.headers.raw()

    const responseHeaders = Object.keys(response.headers.raw()).reduce((acc, h) => {
      // first element of array
      acc[h] = rawHeaders[h][0]
      return acc
    }, {})
    return {
      statusCode: response.status,
      headers: responseHeaders,
      body: await response.text()
    }
  }
}

async function callService (method, headers, body) {
  const actualCall = dummy ? dummyCall : createHTTPCall()
  return actualCall(method, headers, body)
}

module.exports = callService
