function getCredentials (account) {
  const result = {
    bearerTokenCredentials: {
      client_id: process.env.AVCD_CREDS_CLIENT_ID,
      client_secret: process.env.AVCD_CREDS_CLIENT_SECRET
    },
    lessPrivilegedBearerTokenCredentials: {
      client_id: process.env.AVCD_LESS_CREDS_CLIENT_ID,
      client_secret: process.env.AVCD_LESS_CREDS_CLIENT_SECRET
    }
  }
  if (
    result.bearerTokenCredentials.client_id &&
    result.bearerTokenCredentials.client_secret &&
    result.lessPrivilegedBearerTokenCredentials.client_id &&
    result.lessPrivilegedBearerTokenCredentials.client_secret
  ) {
    console.log('Found credentials in environment variables')
    return result[account]
  }

  console.log('Did not find credentials in environment variables. Looking for credentials in credentials.json')
  try {
    const credentials = require('../credentials.json')
    console.log('Found credentials in credentials.json')
    return credentials[account]
  } catch (err) {
    console.error('No credentials found in environment variables, nor credentials.json')
    process.exit(-1)
  }
}

module.exports = getCredentials
