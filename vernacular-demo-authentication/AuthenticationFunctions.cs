using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using System.Net;
using vernacular_demo_authentication.Authorization;

namespace vernacular_demo_authentication
{
    public class AuthenticationFunctions
    {
        private readonly ILogger _logger;

        public AuthenticationFunctions(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<AuthenticationFunctions>();
        }

        [Authorize(Scopes = new[] { Scopes.MyScope }, UserRoles = new[] { UserRoles.User, UserRoles.Admin }, AppRoles = new [] { AppRoles.AccessAllFunctions })]
        [Function("UserAndAdmin")]
        public HttpResponseData UserAndAdmin([HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            var response = req.CreateResponse(HttpStatusCode.OK);
            response.Headers.Add("Content-Type", "text/plain; charset=utf-8");

            response.WriteString("Only user roles and admin roles can see this");

            return response;
        }

        [Authorize(Scopes = new[] { Scopes.MyScope }, UserRoles = new[] { UserRoles.User }, AppRoles = new[] { AppRoles.AccessAllFunctions })]
        [Function("OnlyUser")]
        public HttpResponseData OnlyUser([HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequestData req)
        {
          _logger.LogInformation("C# HTTP trigger function processed a request.");

          var response = req.CreateResponse(HttpStatusCode.OK);
          response.Headers.Add("Content-Type", "text/plain; charset=utf-8");

          response.WriteString("Only user roles can see this");

          return response;
        }

        [Authorize(Scopes = new[] { Scopes.MyScope }, UserRoles = new[] { UserRoles.Admin }, AppRoles = new[] { AppRoles.AccessAllFunctions })]
        [Function("OnlyAdmin")]
        public HttpResponseData OnlyAdmin([HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequestData req)
        {
          _logger.LogInformation("C# HTTP trigger function processed a request.");

          var response = req.CreateResponse(HttpStatusCode.OK);
          response.Headers.Add("Content-Type", "text/plain; charset=utf-8");

          response.WriteString("Only admin roles can see this");

          return response;
        }
    }
}
