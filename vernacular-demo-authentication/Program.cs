using Microsoft.Extensions.Hosting;

using vernacular_demo_authentication.Middleware;

namespace vernacular_demo_authentication
{
    public class Program
    {
        public static void Main()
        {
            var host = new HostBuilder()
              .ConfigureFunctionsWorkerDefaults((context, builder) =>
              {
                  builder.UseMiddleware<AuthenticationMiddleware>();
                  builder.UseMiddleware<AuthorizationMiddleware>();
              })
                .Build();

            host.Run();
        }
    }
}
