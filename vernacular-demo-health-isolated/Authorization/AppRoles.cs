namespace vernacular_demo_health_isolated.Authorization
{
    internal static class AppRoles
    {
        public const string AccessAllFunctions = "Functions.Access.All";
    }
}