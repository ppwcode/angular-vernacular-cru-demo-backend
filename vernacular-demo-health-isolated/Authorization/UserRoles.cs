﻿namespace vernacular_demo_health_isolated.Authorization
{
    internal class UserRoles
    {
        public const string User = "Functions.Access.Users";
        public const string Admin = "Functions.Access.Admins";
    }
}
