﻿using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;

namespace vernacular_demo_health_isolated
{
    public static class CustomLogger
    {
        public static void LogInformation(this ILogger log, HttpRequestData req, string message)
        {
            log.LogInformation("x-flow-id={xFlowId}, message={logMessage}", req.Headers.GetValues("x-flow-id"), message);
        }
    }
}
