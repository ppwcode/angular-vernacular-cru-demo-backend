using Microsoft.Azure.Functions.Worker.Extensions.OpenApi.Extensions;
using Microsoft.Extensions.Hosting;
using vernacular_demo_health_isolated.Middleware;

namespace vernacular_demo_health_isolated
{
    public class Program
    {
        public static void Main()
        {
            var host = new HostBuilder()
              .ConfigureFunctionsWorkerDefaults((context, builder) =>
                {
                    builder.UseMiddleware<AuthenticationMiddleware>();
                    builder.UseMiddleware<AuthorizationMiddleware>();
                })
              .ConfigureOpenApi()
              .Build();

            host.Run();
        }
    }
}
