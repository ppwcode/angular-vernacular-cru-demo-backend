/*
using Azure.Storage.Blobs;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using vernacular_demo_health_isolated.Models;

namespace vernacular_demo_health_isolated
{
    public class Persons
    {
        private readonly ILogger _logger;

        public Persons(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<Persons>();
        }

        [OpenApiOperation("Persons", "Persons", Summary = "Creates Person object", Description = "This method creates a Person object")]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter("x-flow-id", In = ParameterLocation.Header, Required = true, Type = typeof(string))]
        [OpenApiRequestBody("application/json", typeof(Person))]
        [OpenApiResponseWithBody(HttpStatusCode.Created, "application/json", typeof(Person), Summary = "Person object", Description = "Returns the created Person object")]
        [OpenApiResponseWithBody(HttpStatusCode.BadRequest, "application/json", typeof(CustomError), Summary = "Error message", Description = "Returns JSON object containing the error message")]
        [Function("CreatePerson")]
        public async Task<MultiResponse> CreatePerson(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "persons")]
            HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");
            _logger.LogInformation(req, "Log from backend");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            Person person = JsonConvert.DeserializeObject<Person>(requestBody);

            var response = req.CreateResponse();

            if (person != null && person.Name.Length > 10)
            {
                await response.WriteAsJsonAsync(new CustomError("Name is longer than 10 characters."),
                    HttpStatusCode.BadRequest);

                return new MultiResponse
                {
                    HttpResponse = response
                };
            }

            await response.WriteAsJsonAsync(person, HttpStatusCode.Created);

            return new MultiResponse
            {
                Person = person,
                HttpResponse = response
            };
        }

        [OpenApiOperation("Persons", "Persons", Summary = "Retrieves person object by id", Description = "This method gets a Person object by id")]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter("id", In = ParameterLocation.Path, Required = true, Type = typeof(int))]
        [OpenApiResponseWithBody(HttpStatusCode.OK, "application/json", typeof(Person), Summary = "Person object", Description = "Returns the requested Person object")]
        [Function("GetPerson")]
        public async Task<HttpResponseData> GetPerson(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "persons/{id}")]
            HttpRequestData req, int id)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            Person person = new Person(id, "Test");

            var response = req.CreateResponse();
            await response.WriteAsJsonAsync(person);

            return response;
        }

        [Function("ProcessPerson")]
        [BlobOutput("vernacular-demo-health-isolated-archive/{name}")]
        public async Task<Person> ProcessPerson(
            [BlobTrigger("vernacular-demo-health-isolated/{name}")]
            string personBlob, string name)
        {
            _logger.LogInformation($"C# Blob trigger function Processed blob\n Name: {name} \n Data: {personBlob}");

            Person person = JsonConvert.DeserializeObject<Person>(personBlob);
            person.Name += "_Processed";

            BlobServiceClient serviceClient = new BlobServiceClient(Environment.GetEnvironmentVariable("AzureWebJobsStorage"));
            BlobContainerClient sourceContainer = serviceClient.GetBlobContainerClient("vernacular-demo-health-isolated");
            await sourceContainer.DeleteBlobIfExistsAsync(name);

            _logger.LogInformation("Id={0},Name={1}", person.Id, person.Name);

            return person;
        }
    }
}
*/
