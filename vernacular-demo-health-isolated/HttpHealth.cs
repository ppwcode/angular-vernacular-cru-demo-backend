using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using vernacular_demo_health_isolated.Authorization;

namespace vernacular_demo_health_isolated
{
    public class HttpHealth
    {
        private readonly ILogger _logger;

        public HttpHealth(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<HttpHealth>();
        }

        [OpenApiOperation("Health", "Health", Summary = "Ping API", Description = "This method checks the API status")]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter("x-flow-id", In = ParameterLocation.Header, Required = true, Type = typeof(string))]
        [OpenApiParameter("x-mode", In = ParameterLocation.Header, Required = true, Type = typeof(string))]
        [OpenApiParameter("x-force-error", In = ParameterLocation.Header, Required = false, Type = typeof(string))]
        [OpenApiResponseWithoutBody(HttpStatusCode.OK, Summary = "Ping API", Description = "Returns status code 200 if endpoint is healthy")]
        [Authorize(Scopes = new[] {Scopes.MyScope}, UserRoles = new[] {UserRoles.User}, AppRoles = new[] {AppRoles.AccessAllFunctions})]
        [Function("HttpHealth")]
        public async Task<HttpResponseData> Run([HttpTrigger(AuthorizationLevel.Function, "get", Route = "I/health")] HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            HttpResponseData res = req.CreateResponse(HttpStatusCode.OK);

            if (req.Headers.Contains("x-force-error"))
            {
                string errorCode = req.Headers.GetValues("x-force-error").First();
                res = req.CreateResponse((HttpStatusCode) int.Parse(errorCode));
                res.Headers.Add("x-flow-id", req.Headers.GetValues("x-flow-id"));
                res.Headers.Add("x-mode", req.Headers.GetValues("x-mode"));
                res.Headers.Add("x-force-error", req.Headers.GetValues("x-force-error"));
                res.Headers.Add("x-date", DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"));
                res.Headers.Add("cache-control", "no-store");
                return res;
            }

            await res.WriteAsJsonAsync(new {structureVersion = 1, status = "OK", db = "OK"}, HttpStatusCode.OK);
            res.Headers.Add("x-flow-id", req.Headers.GetValues("x-flow-id"));
            res.Headers.Add("x-mode", req.Headers.GetValues("x-mode"));
            res.Headers.Add("x-date", DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"));
            res.Headers.Add("cache-control", "no-store");
            return res;
        }
    }
}