namespace vernacular_demo_health_isolated.Models
{
    public class CustomError
    {
        public string Message { get; set; }

        public CustomError(string message)
        {
            Message = message;
        }
    }
}
