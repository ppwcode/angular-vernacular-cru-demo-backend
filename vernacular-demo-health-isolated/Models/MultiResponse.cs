using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;

namespace vernacular_demo_health_isolated.Models
{
    public class MultiResponse
    {
        [BlobOutput("vernacular-demo-health-isolated/{name}", Connection = "AzureWebJobsStorage")]
        public Person Person { get; set; }
        public HttpResponseData HttpResponse { get; set; }
    }
}
