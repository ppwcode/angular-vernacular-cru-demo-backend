using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using azure_front_door_employers.Models;

namespace azure_front_door_employers
{
    public class GetEmployer
    {
        private readonly ILogger _logger;

        public GetEmployer(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<GetEmployer>();
        }

        [Function("GetEmployer")]
        public async Task<HttpResponseData> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "I/employers/{id}")] HttpRequestData req, int id)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            Employer employer = new Employer(id, "Kristof", "De Winter");

            var response = req.CreateResponse();
            await response.WriteAsJsonAsync(employer);

            return response;
        }
    }
}
