﻿using Microsoft.Azure.Functions.Worker.Http;

namespace azure_front_door_employers.Models
{
    public class MultiResponse
    {
        public Employer Employer { get; set; }
        public HttpResponseData HttpResponse { get; set; }
    }
}
