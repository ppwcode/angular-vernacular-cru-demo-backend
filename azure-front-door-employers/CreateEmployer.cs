using azure_front_door_employers.Models;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace azure_front_door_employers
{
    public class CreateEmployer
    {
        private readonly ILogger _logger;

        public CreateEmployer(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<CreateEmployer>();
        }

        [Function("CreateEmployer")]
        public async Task<MultiResponse> Run([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "I/employers")] HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            Employer employer = JsonConvert.DeserializeObject<Employer>(requestBody);

            var response = req.CreateResponse();
            await response.WriteAsJsonAsync(employer, HttpStatusCode.Created);

            return new MultiResponse
            {
                Employer = employer,
                HttpResponse = response
            };
        }
    }
}
