using azure_front_door_employees.Models;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace azure_front_door_employees
{
    public class GetEmployee
    {
        private readonly ILogger _logger;

        public GetEmployee(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<GetEmployee>();
        }

        [Function("GetEmployee")]
        public async Task<HttpResponseData> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "I/employees/{id}")] HttpRequestData req, int id)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            Employee employee = new Employee(id, "Kristof", "De Winter");

            var response = req.CreateResponse();
            await response.WriteAsJsonAsync(employee);

            return response;
        }
    }
}
