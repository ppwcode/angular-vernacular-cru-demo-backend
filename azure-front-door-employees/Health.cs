using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using System.Net;

namespace azure_front_door_employees
{
    public class Health
    {
        private readonly ILogger _logger;

        public Health(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<Health>();
        }

        [Function("Health")]
        public HttpResponseData Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "I/health")] HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            var response = req.CreateResponse(HttpStatusCode.OK);
            return response;
        }
    }
}
