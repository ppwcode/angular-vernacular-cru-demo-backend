﻿using Microsoft.Azure.Functions.Worker.Http;

namespace azure_front_door_employees.Models
{
    public class MultiResponse
    {
        public Employee Employee { get; set; }
        public HttpResponseData HttpResponse { get; set; }
    }
}
