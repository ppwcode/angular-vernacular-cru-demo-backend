using azure_front_door_employees.Models;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace azure_front_door_employees
{
    public class CreateEmployee
    {
        private readonly ILogger _logger;

        public CreateEmployee(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<CreateEmployee>();
        }

        [Function("CreateEmployee")]
        public async Task<MultiResponse> Run([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "I/employees")] HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            Employee employee = JsonConvert.DeserializeObject<Employee>(requestBody);

            var response = req.CreateResponse();
            await response.WriteAsJsonAsync(employee, HttpStatusCode.Created);

            return new MultiResponse
            {
                Employee = employee,
                HttpResponse = response
            };
        }
    }
}
